package main

import (
	"data_landfill/snowflake"
	"math/rand"
	"time"
)

func main() {
	// seed the RNG
	rand.NewSource(time.Now().UnixNano())

	snowflake.Main()
}

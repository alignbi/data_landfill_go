// Package faker contains fake data generating functions
// and also implements third-party library "github.com/manveru/faker"
package faker

import (
	"fmt"
	"github.com/manveru/faker"
	"github.com/nu7hatch/gouuid"
	"math/rand"
	"strconv"
	"strings"
)

var Fake, Err = faker.New("en")

// Returns ranom int up to passed in int as string
func ID(id int) string {
	return strconv.Itoa(rand.Intn(id + 1))
}

// Returns IPv4 address as a string
func IPAddress() string {
	oct := func() int { return 2 + rand.Intn(254) }
	return fmt.Sprintf("%d.%d.%d.%d", oct(), oct(), oct(), oct())
}

// Returns random item of array, should be passed array of "results" that are strings
func Result(r []string) string {
	return r[rand.Intn(len(r))]
}

// Returns a UUID as a string
func UUID() string {
	u, err := uuid.NewV4()
	if err != nil {
		panic(err)
	}
	return u.String()
}

// string arrays needed for URI function
var (
	results       = []string{}
	uriPaths      = []string{"app", "main", "wp-content", "search", "category", "tag", "categories", "tags", "blog", "posts", "list", "explore"}
	uriPages      = []string{"index", "home", "search", "main", "post", "homepage", "category", "register", "login", "faq", "about", "terms", "privacy", "author"}
	uriExtensions = []string{".html", ".html", ".html", ".htm", ".htm", ".php", ".php", ".jsp", ".asp"}
)

// Returns random URI based on URI slices
func URI() string {
	fullPath := []string{}
	numOfDirs := rand.Intn(2)
	// Add directories
	switch {
	case numOfDirs > 1:
		fullPath = append(fullPath, fmt.Sprintf("/%s/%s",
			randChoice(uriPaths),
			randChoice(uriPaths)))
	case numOfDirs == 1:
		fullPath = append(fullPath, fmt.Sprintf("/%s",
			randChoice(uriPaths)))
	case numOfDirs == 0:
		return fmt.Sprintf("/%s%s",
			randChoice(uriPages),
			randChoice(uriExtensions))
	}
	fullPath = append(fullPath, fmt.Sprintf("%s%s",
		randChoice(uriPages),
		randChoice(uriExtensions)))
	return strings.Join(fullPath, "/")
}

// vars needed for Date function
var (
	year    = 2016
	month   = 05
	days    = 31
	hours   = 24
	minSecs = 60
)

// Returns random date string like "2016-04-13 02:33:55"
// Currently set to May 2016
func Date() string {
	switch month {
	case 2:
		days = 28
	case 4, 6, 9, 11:
		days = 30
	}

	day := rand.Intn(days) + 1
	hour := rand.Intn(hours) + 1
	min := rand.Intn(minSecs)
	sec := rand.Intn(minSecs)

	datetime := fmt.Sprintf("%d-%02d-%02d %02d:%02d:%02d",
		year,
		month,
		day,
		hour,
		min,
		sec)
	return datetime
}

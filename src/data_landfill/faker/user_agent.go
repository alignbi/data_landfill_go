// For generating a random user agent
package faker

import (
	"fmt"
	"math/rand"
)

var (
	user_agents = []string{
		"chrome", "firefox", "internet_explorer", "opera", "safari"}
	windows_platform_tokens = []string{
		"Windows 95", "Windows 98", "Windows 98; Win 9x 4.90", "Windows CE",
		"Windows NT 4.0", "Windows NT 5.0", "Windows NT 5.01",
		"Windows NT 5.1", "Windows NT 5.2", "Windows NT 6.0", "Windows NT 6.1",
		"Windows NT 6.2"}
	linux_processors = []string{"i686", "x86_64"}
	mac_processors   = []string{"Intel", "PPC", "U; Intel", "U; PPC"}
	langs            = []string{"en-US", "sl-SI", "it-IT"}
)

func randRange(min, max int) int {
	return rand.Intn(max+1-min) + min
}

func randLang() string {
	return langs[rand.Intn(len(langs))]
}

func randChoice(s []string) string {
	return s[rand.Intn(len(s))]
}

func mac_processor() string {
	return randChoice(mac_processors)
}

func linux_processor() string {
	return randChoice(linux_processors)
}

func windowsPlatToken() string {
	return randChoice(windows_platform_tokens)
}

func linuxPlatToken() string {
	return fmt.Sprintf("X11; Linux %s",
		randChoice(linux_processors))
}

func macPlatToken() string {
	return fmt.Sprintf("Macintosh; %s Mac OS X 10_%d_%d",
		randChoice(mac_processors),
		randRange(5, 8),
		rand.Intn(9))
}

func User_Agent() string {
	switch rand.Intn(5) {
	case 0:
		return chrome()
	case 1:
		return firefox()
	case 2:
		return safari()
	case 3:
		return internet_explorer()
	case 4:
		return opera()
	default:
		return ""
	}
}

func chrome() string {
	switch rand.Intn(3) {
	case 0:
		return chrome_gen(linuxPlatToken())
	case 1:
		return chrome_gen(macPlatToken())
	case 2:
		return chrome_gen(windowsPlatToken())
	default:
		return ""
	}
}

func chrome_gen(pt string) string {
	saf := fmt.Sprintf("%d%d", randRange(531, 536), rand.Intn(2))
	return fmt.Sprintf("Mozilla/5.0 (%s) AppleWebKit/%s (KHTML, like Gecko) Chrome/%d.0.%d.0 Safari/%s",
		pt,
		saf,
		randRange(13, 15),
		randRange(800, 899),
		saf)
}

func firefox() string {
	vers := []string{
		fmt.Sprintf("Gecko/%d%d%d Firefox/%d.0",
			randRange(2011, 2016),
			randRange(1, 12),
			randRange(1, 31),
			randRange(4, 15)),
		fmt.Sprintf("Gecko/%d%d%d Firefox/3.6.%d",
			randRange(2010, 2016),
			randRange(1, 12),
			randRange(1, 31),
			randRange(4, 15)),
		fmt.Sprintf("Gecko/%d%d%d Firefox/3.8",
			randRange(2010, 2016),
			randRange(1, 12),
			randRange(1, 31))}
	switch rand.Intn(3) {
	case 0:
		// linux
		return fmt.Sprintf("Mozilla/5.0 (%s; rv:1.9.%d.20) %s",
			linuxPlatToken(),
			randRange(5, 7),
			randChoice(vers))
	case 1:
		// mac
		return fmt.Sprintf("Mozilla/5.0 (%s; rv:1.9.%d.20) %s",
			macPlatToken(),
			randRange(2, 6),
			randChoice(vers))
	case 2:
		// win
		return fmt.Sprintf("Mozilla/5.0 (%s; %s; rv:1.9.%d.20) %s",
			windowsPlatToken(),
			randLang(),
			rand.Intn(3),
			randChoice(vers))
	default:
		return ""
	}
}

func safari() string {
	saf := fmt.Sprintf("%d.%d.%d",
		randRange(531, 535),
		randRange(1, 50),
		randRange(1, 7))
	switch rand.Intn(3) {
	case 0:
		// mac
		templ := "Mozilla/5.0 (%s rv:%d.0; %s) AppleWebKit/%s (KHTML, like Gecko) Version/%s Safari/%s"
		return fmt.Sprintf(templ,
			macPlatToken(),
			randRange(2, 6),
			randLang(),
			saf,
			safari_ver(),
			saf)
	case 1:
		// ipod
		templ := "Mozilla/5.0 (iPod; U; CPU iPhone OS %d_%d like Mac OS X; %s) AppleWebKit/%s (KHTML, like Gecko) Version/%s.0.5 Mobile/8B%s Safari/6{6}"
		return fmt.Sprintf(templ,
			randRange(3, 4),
			randRange(0, 3),
			randLang(), saf,
			safari_ver(), saf)
	case 2:
		// win
		templ := "Mozilla/5.0 (Windows; U; %s) AppleWebKit/%s (KHTML, like Gecko) Version/%s Safari/%s"
		return fmt.Sprintf(templ,
			windowsPlatToken(),
			saf, safari_ver(), saf)
	default:
		return ""
	}
}

func safari_ver() string {
	switch rand.Intn(2) {
	case 0:
		return fmt.Sprintf("%d.%d",
			randRange(4, 5),
			rand.Intn(2))
	default:
		return fmt.Sprintf("%d.0.%d",
			randRange(4, 5),
			randRange(1, 5))
	}
}

func opera() string {
	return fmt.Sprintf("Opera/%d.%d.%s",
		randRange(8, 9),
		randRange(10, 99),
		opera_gen())
}

func opera_gen() string {
	templ := "(%s; %s) Presto/2.9.%d Version/%d.00"
	switch rand.Intn(2) {
	case 0:
		return fmt.Sprintf(templ,
			linuxPlatToken(),
			randLang(),
			randRange(160, 190),
			randRange(10, 12))
	default:
		return fmt.Sprintf(templ,
			windowsPlatToken(),
			randLang(),
			randRange(160, 190),
			randRange(10, 12))
	}
}

func internet_explorer() string {
	return fmt.Sprintf("Mozilla/5.0 (compatible; MSIE %d.0; %s; Trident/%d.%d)",
		randRange(5, 9),
		windowsPlatToken(),
		randRange(3, 5),
		rand.Intn(2))
}

// Package snowball for generating data required for snowball test run
package snowflake

import (
	"data_landfill/faker"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
)

// Generic Table
type Table struct {
	FilePrefix  string
	Record      []string
	incrId      int
	rowCount    int
	writeHeader bool
}

// A constructor for a Table struct
func NewTable(fp string, r []string, rc int) *Table {
	return &Table{
		FilePrefix: fp,
		Record:     r,
		rowCount:   rc,
	}
}

// csv writing helper function
func writeCSV(p string, t *Table) {
	csvNum := 0
	rowNum := 0
	writeHeader := true
	writingRows := true
	for writingRows {
		// Open new file
		absPath := fmt.Sprintf("%s%s_%04d.csv", p, t.FilePrefix, csvNum)
		f, err := os.Create(absPath)
		if err != nil {
			panic(err)
		}
		defer f.Close()

		// Create csvWriter
		w := csv.NewWriter(f)
		start := time.Now()
		fmt.Println("Writing file:", absPath)

		// Write header if needed
		if writeHeader {
			writeLine(t, w)
			writeHeader = false
		}

		// Write rows
		for i := rowNum; i < t.rowCount; i++ {
			rowNum++
			t.UpdateRecord()
			writeLine(t, w)
			pathStat, err := os.Stat(absPath)
			if err != nil {
				panic(err)
			}
			// check if csv file has reached 100 MiB in size.
			if pathStat.Size() >= 104857600 {
				csvNum++
				break
			}
		}
		w.Flush()
		if err := w.Error(); err != nil {
			log.Fatal(err)
		}
		elapsed := time.Since(start)
		log.Printf("Time spent writing %s: %s", absPath, elapsed)
		if rowNum >= t.rowCount {
			fmt.Println("Done!")
			writingRows = false
		}
	}
}

func writeLine(t *Table, w *csv.Writer) {
	if err := w.Write(t.Record); err != nil {
		log.Fatalln("Error writing record to csv:", err)
	}
}

var results []string

// Error checking helper function
func check(e error) {
	if e != nil {
		panic(e)
	}
}

// Increments a Table's incrId, and updates a Table's record depending on which table it represents
func (t *Table) UpdateRecord() {
	t.incrId++
	switch t.FilePrefix {
	case "Fact_Events":
		t.Record = []string{faker.Date(), faker.ID(100), faker.ID(1000000), faker.IPAddress(), faker.URI(), faker.User_Agent(), faker.Result(results)}
	case "Dim_Accounts":
		t.Record = []string{strconv.Itoa(t.incrId), faker.UUID(), faker.Fake.Name(), faker.Fake.Country()}
	case "Dim_Events":
		t.Record = []string{strconv.Itoa(t.incrId), fmt.Sprintf("Event_%03d", t.incrId), fmt.Sprintf("Category_%03d", t.incrId)}
	}
}

func Main() {
	check(faker.Err)

	//build up results slice. Like an application with 99.9% availability
	for i := 0; i < 1000; i++ {
		if i < 999 {
			results = append(results, "200")
		} else {
			results = append(results, "500")
		}
	}

	path := "/tmp/"
	fePrefix := "Fact_Events"
	feHeader := []string{"Event_Date", "Event_Id", "Account_Id", "IP_Address", "URI", "User_Agent", "Result"}
	fe := NewTable(fePrefix, feHeader, 1000000000)

	daPrefix := "Dim_Accounts"
	daHeader := []string{"Account_Id", "Account_Uuid", "Account_Name", "Account_Country"}
	da := NewTable(daPrefix, daHeader, 1000000)

	dePrefix := "Dim_Events"
	deHeader := []string{"Event_Id", "Event_Name", "Event_Category"}
	de := NewTable(dePrefix, deHeader, 100)

	tables := []*Table{fe, da, de}

	for _, table := range tables {
		writeCSV(path, table)
	}
}

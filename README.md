# data_landfill

Generate large amounts of fake data and dump to csv files. Implemented in Go.

## Usage
Run with 'go run' command. From in the data_landfill_go/src/data_landfill directory:

    $ go run main/main.go
    Writing file: /tmp/Fact_Events_0000.csv
    2016/05/06 13:39:24 Time spent writing /tmp/Fact_Events_0000.csv: 10.865099024s
    Writing file: /tmp/Fact_Events_0001.csv
    2016/05/06 13:39:36 Time spent writing /tmp/Fact_Events_0001.csv: 11.122635912s
    Writing file: /tmp/Fact_Events_0002.csv
    2016/05/06 13:39:46 Time spent writing /tmp/Fact_Events_0002.csv: 10.629928065s
    Writing file: /tmp/Fact_Events_0003.csv
    2016/05/06 13:39:57 Time spent writing /tmp/Fact_Events_0003.csv: 10.890916093s
    Writing file: /tmp/Fact_Events_0004.csv
    2016/05/06 13:40:01 Time spent writing /tmp/Fact_Events_0004.csv: 3.803561972s
    Done!
    Writing file: /tmp/Dim_Accounts_0000.csv
    2016/05/06 13:40:01 Time spent writing /tmp/Dim_Accounts_0000.csv: 1.911769ms
    Done!
    Writing file: /tmp/Dim_Events_0000.csv
    2016/05/06 13:40:01 Time spent writing /tmp/Dim_Events_0000.csv: 446.757µs
    Done!

This is a work in progress.. Currently just writes data for the snowflake evaluation to CSV files in /tmp
